# Simple chat #

[![Build Status](https://www.bitrise.io/app/6950872032664bad.svg?token=fn0genEabWsP0CLfO21mkw&branch=master)](https://www.bitrise.io/app/6950872032664bad)

Simple chat is an app that allows user to input a chat message string and convert it to JSON string containing information about its contents. Special content to look for includes: @mentions, (emoticons) and links.
I tried to achieve two goals in this project: build a production-ready app and keep source code small and straightforward. 

![demo.gif](https://bitbucket.org/repo/KxABAB/images/4187214614-demo.gif)

### Project structure ###

*SimpleChat* group contains Simple chat application written in Objective-C. There are two subgroups inside: *MessageParser* and *TitleLoader*. These components don't have dependencies on each other and main application code so that they could be reused easily in other projects.
*SimpleChatTests* group contains unit tests written in Swift. Using Swift for unit tests is my way of learning Swift's best practices without risk of breaking production code.

### Building application ###

Xcode 7 is recommended to build and run application and unit tests.
Xcode 6 is enough to run the application (without unit tests).
Simple app is a universal app and runs on both iPhone and iPad.
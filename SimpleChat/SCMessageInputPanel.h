//
//  SCMessageTextView.h
//  SimpleChat
//
//  Created by Anton Soloviev on 17/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SCMessageInputPanel;

@protocol SCMessageInputPanelProtocol <NSObject>
- (void)send:(SCMessageInputPanel *)inputPanel;
@optional
- (void)textDidChange:(SCMessageInputPanel *)inputPanel;
@end


@interface SCMessageInputPanel : UIView <UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIButton *sendBtn;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *minHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *maxHeightConstraint;
@property (nonatomic, weak) IBOutlet id<SCMessageInputPanelProtocol> delegate;

@end

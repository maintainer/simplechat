//
//  main.m
//  SimpleChat
//
//  Created by Anton Soloviev on 16/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

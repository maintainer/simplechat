//
//  SCChatController.m
//  SimpleChat
//
//  Created by Anton Soloviev on 21/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import "SCChatController.h"
#import "SCTitleLoader.h"
#import "SCMessageParser.h"


@interface SCChatController ()
@property (nonatomic, strong) NSMutableArray *messagesJSON; // Cache for generated JSON strings
@end


@implementation SCChatController {
    SCTitleLoader *_titleLoader;
    NSMutableArray *_messages;
}

- (instancetype)initWithDelegate:(id<SCChatControllerDelegate>) delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        _titleLoader = [SCTitleLoader new];
        _messages = [NSMutableArray new];
        _messagesJSON = [NSMutableArray new];
    }
    return self;
}

- (void)dealloc {
    [_titleLoader invalidateSession];
}

- (void)sendMessage:(NSString*)message {
    SCMessageParser *parser = [[SCMessageParser alloc] initWithMessage:message];
    SCMessageInfo *messageInfo = [parser parse];
    [_messages addObject:messageInfo];
    NSUInteger messageIndex = _messages.count - 1;
    
    [_messagesJSON addObject:[self generateJSON:messageInfo]];
    
    if (messageInfo.links.count > 0) {
        for (SCLinkInfo *linkInfo in messageInfo.links) {
            messageInfo.tasksCounter++;
            __weak SCChatController *weakSelf = self;
            [_titleLoader loadTitleForURL:linkInfo.linkURL completion:^(NSString *title) {
                __strong SCChatController *self = weakSelf;
                NSString *messageLink = [linkInfo.linkURL copy];
                NSUInteger index = messageIndex;
                if (self) {
                    SCMessageInfo *messageInfo = [self getMessageInfo:index];
                    messageInfo.tasksCounter--;
                    for (SCLinkInfo *linkInfo in messageInfo.links) {
                        if ([linkInfo.linkURL isEqualToString:messageLink]) {
                            linkInfo.linkTitle = title;
                            [self.messagesJSON replaceObjectAtIndex:index withObject:[self generateJSON:messageInfo]];
                            [self.delegate updateMessage:index];
                            break;
                        }
                    }
                }
            }];
        }
    }
}

- (NSString *)generateJSON:(SCMessageInfo *)messageInfo {
    NSMutableDictionary *messageDict = [NSMutableDictionary dictionaryWithCapacity:3];
    if (messageInfo.mentions.count > 0) {
        [messageDict setValue:messageInfo.mentions forKey:@"mentions"];
    }
    if (messageInfo.emoticons.count > 0) {
        [messageDict setValue:messageInfo.emoticons forKey:@"emoticons"];
    }
    if (messageInfo.links.count > 0) {
        NSMutableArray *linksArray = [NSMutableArray arrayWithCapacity:messageInfo.links.count];
        for (SCLinkInfo *linkInfo in messageInfo.links) {
            NSMutableDictionary *linkDict = [NSMutableDictionary dictionaryWithDictionary:@{@"url": linkInfo.linkURL}];
            if (linkInfo.linkTitle != nil) {
                [linkDict setValue:linkInfo.linkTitle forKey:@"title"];
            }
            [linksArray addObject:linkDict];
        }
        [messageDict setValue:linksArray forKey:@"links"];
    }
    
    NSError *err;
    NSData *data = [NSJSONSerialization dataWithJSONObject:messageDict
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:&err];
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (NSUInteger)getMessagesCount {
    return _messages.count;
}

- (NSString*) getMessageJSON:(NSUInteger) index {
    return _messagesJSON[index];
}

- (SCMessageInfo*)getMessageInfo:(NSUInteger) index {
    return _messages[index];
}

@end

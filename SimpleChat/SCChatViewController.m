//
//  ViewController.m
//  SimpleChat
//
//  Created by Anton Soloviev on 16/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import "SCChatViewController.h"
#import "SCMessageCell.h"
#import "SCChatController.h"


#define MESSAGE_CELL @"MESSAGE_CELL"

@implementation SCChatViewController {
    SCChatController *_chatController;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _chatController = [[SCChatController alloc] initWithDelegate:self];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _chatController = [[SCChatController alloc] initWithDelegate:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:UIKeyboardWillChangeFrameNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:UIKeyboardWillHideNotification];
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

#pragma mark - Keyboard events

- (void)keyboardWillChangeFrame:(NSNotification*)notification {
    // Keyboard will be shown or resized
    NSDictionary *userInfo = [notification userInfo];
    CGRect kbSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat kbHight = kbSize.size.height;
    
    _textViewBottomConstraint.constant = kbHight;
    
    NSTimeInterval keyboardAnimationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:keyboardAnimationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification*)notification {
    NSDictionary *userInfo = [notification userInfo];
    
    _textViewBottomConstraint.constant = 0;
    
    NSTimeInterval kbAnimationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:kbAnimationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Table data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_chatController getMessagesCount];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *messageJSON = [_chatController getMessageJSON:indexPath.row];
    NSString *messageText = [_chatController getMessageInfo:indexPath.row].message;
    
    const CGFloat labelWidth = tableView.frame.size.width - 40.; // 40 is margins set with constraints in story board
    UIFont *font = [UIFont systemFontOfSize:15.];
    NSAttributedString *messageTextAttr = [[NSAttributedString alloc] initWithString:messageText
                                                               attributes:@{NSFontAttributeName: font}];
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    CGRect messageRect = [messageTextAttr boundingRectWithSize:CGSizeMake(labelWidth, CGFLOAT_MAX)
                                             options:options
                                             context:nil];

    NSAttributedString *messageJSONAttr = [[NSAttributedString alloc] initWithString:messageJSON
                                                                  attributes:@{NSFontAttributeName: font}];
    CGRect messageJSONRect = [messageJSONAttr boundingRectWithSize:CGSizeMake(labelWidth, CGFLOAT_MAX)
                                                           options:options
                                                           context:nil];
    
    return ceil(messageRect.size.height + messageJSONRect.size.height + 40.);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SCMessageCell *messageCell = [tableView dequeueReusableCellWithIdentifier:MESSAGE_CELL
                                                                 forIndexPath:indexPath];
    
    SCMessageInfo *messageInfo = [_chatController getMessageInfo:indexPath.row];
    messageCell.messageLabel.text  = [messageInfo message];
    messageCell.infoLabel.text  = [_chatController getMessageJSON:indexPath.row];
    
    if (messageInfo.tasksCounter > 0) {
        [messageCell.activityIndicator startAnimating];
    } else {
        [messageCell.activityIndicator stopAnimating];
    }
    
    return messageCell;
}

#pragma mark - Input panel delegate

- (void)send:(SCMessageInputPanel *)inputPanel {
    [_chatController sendMessage:inputPanel.textView.text];
    
    NSIndexPath *lastRow = [NSIndexPath indexPathForRow:[_chatController getMessagesCount]-1 inSection:0];
    [_messagesTable insertRowsAtIndexPaths:@[lastRow] withRowAnimation:UITableViewRowAnimationFade];
    
    [_messagesTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_chatController getMessagesCount]-1 inSection:0]
                          atScrollPosition:UITableViewScrollPositionBottom
                                  animated:YES];
}

#pragma mark -

- (void)updateMessage:(NSUInteger)messageIndex {
    [_messagesTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:messageIndex inSection:0]]
                          withRowAnimation:UITableViewRowAnimationNone];
    
    [_messagesTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_chatController getMessagesCount]-1 inSection:0]
                          atScrollPosition:UITableViewScrollPositionBottom
                                  animated:YES];
}

@end

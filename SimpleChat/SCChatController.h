//
//  SCChatController.h
//  SimpleChat
//
//  Created by Anton Soloviev on 21/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCMessageInfo.h"


@protocol SCChatControllerDelegate <NSObject>
- (void)updateMessage:(NSUInteger) messageIndex;
@end


@interface SCChatController : NSObject

@property (nonatomic, weak) id<SCChatControllerDelegate> delegate;

- (instancetype)initWithDelegate:(id<SCChatControllerDelegate>) delegate;

/**
 *  Send message to chat.
 *
 *  @param message Message text.
 */
- (void)sendMessage:(NSString*) message;

/**
 *  Total messages count in chat
 *
 *  @return Messages count.
 */
- (NSUInteger)getMessagesCount;

/**
 *  Get metadata structure for the message with specific index.
 *
 *  @param index A message index.
 *
 *  @return A SCMessageInfo instance with metadata.
 */
- (SCMessageInfo*)getMessageInfo:(NSUInteger) index;

/**
 *  Get JSON representatin of metadata for the message with specific index.
 *
 *  @param index A message index.
 *
 *  @return A string with JSON-encoded metadata.
 */
- (NSString*)getMessageJSON:(NSUInteger) index;

@end

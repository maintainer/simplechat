//
//  SCMessageInfo
//  SimpleChat
//
//  Created by Anton Soloviev on 17/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCLinkInfo.h"


@interface SCMessageInfo : NSObject

/**
 *  The original message text
 */
@property (nonatomic, copy) NSString *message;

/**
 *  Array of NSString objects with user names without '@' character
 */
@property (nonatomic, strong) NSMutableArray *mentions;

/**
 *  Array of NSString objects with emoticons without parentheses
 */
@property (nonatomic, strong) NSMutableArray *emoticons;

/**
 *  Array of SCLinkInfo objects with URLs and titles
 */
@property (nonatomic, strong) NSMutableArray *links;

/**
 *  Number of running asyncronous tasks for this message;
 */
@property (atomic, assign) NSUInteger tasksCounter;

@end

//
//  SCMessageParser.m
//  SimpleChat
//
//  Created by Anton Soloviev on 17/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import "SCMessageParser.h"
#import "SCMessageInfo.h"


static const NSInteger kMaxEmoticonLength = 15;


@implementation SCMessageParser {
    NSScanner *_scanner;
    SCMessageInfo *_messageInfo;
}

- (instancetype)initWithMessage:(NSString*) message {
    self = [super init];
    if (self) {
        _scanner = [NSScanner localizedScannerWithString:message];
        [_scanner setCharactersToBeSkipped:[NSCharacterSet illegalCharacterSet]];
    }
    return self;
}

- (SCMessageInfo*)parse {
    if (_messageInfo) {
        return _messageInfo;
    }
    _messageInfo = [SCMessageInfo new];
    _messageInfo.message = _scanner.string;
    
    // Start characters set for @mentions, (emoticons) and http(s) links
    NSCharacterSet *startCharacters = [NSCharacterSet characterSetWithCharactersInString:@"@(hH"];
    
    while (!_scanner.isAtEnd) {
        [_scanner scanUpToCharactersFromSet:startCharacters intoString:NULL];
        
        if (_scanner.isAtEnd) {
            break;
        }
        
        unichar c = [_scanner.string characterAtIndex:_scanner.scanLocation];
        if (![self isAfterSeparatorCharacter]) {      // there is no breaking character before start character
            [_scanner scanCharactersFromSet:startCharacters intoString:NULL]; // so skip this start character
            continue;                                                         // and continue parsing
        }
        
        switch (c) {
            case '@':
                [self extractMention];
                break;
            case '(':
                [self extractEmoticon];
                break;
            case 'h':
            case 'H':
                [self extractLink];
                break;
            default:
                break;
        }
    }
    
    return _messageInfo;
}

- (void)extractMention {
    NSString *mention = nil;
    [_scanner scanString:@"@" intoString:NULL];
    NSUInteger savedLocation = _scanner.scanLocation;
    if ([_scanner scanCharactersFromSet:[NSCharacterSet alphanumericCharacterSet] intoString:&mention]) {
        [_messageInfo.mentions addObject:mention];
        return;
    }
    _scanner.scanLocation = savedLocation;      // not a @mention, rollback scan location
}

- (void)extractEmoticon {
    NSString *emoticon = nil;
    [_scanner scanString:@"(" intoString:NULL];
    NSUInteger savedLocation = _scanner.scanLocation;
    if ([_scanner scanCharactersFromSet:[NSCharacterSet alphanumericCharacterSet] intoString:&emoticon]) {
        if ([_scanner scanString:@")" intoString:NULL] && [emoticon length] <= kMaxEmoticonLength) {
            [_messageInfo.emoticons addObject:emoticon];
            return;
        }
    }
    _scanner.scanLocation = savedLocation;      // not an (emoticon), rollback scan location
}

- (void)extractLink {
    NSUInteger savedLocation = _scanner.scanLocation;
    BOOL hasPrefix = [_scanner scanString:@"http" intoString:NULL] || [_scanner scanString:@"HTTP" intoString:NULL];
    BOOL isHTTPS = [_scanner scanString:@"s" intoString:NULL] || [_scanner scanString:@"S" intoString:NULL];
    
    if (hasPrefix && [_scanner scanString:@"://" intoString:NULL]) {
        NSString *link = nil;
        if ([_scanner scanUpToCharactersFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] intoString:&link]) {
            NSString *result = [NSString stringWithFormat:@"http%@://%@", isHTTPS?@"s":@"", link];
            NSURL *potentialURL = [NSURL URLWithString:[result stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            if (potentialURL && [potentialURL host]) {
                SCLinkInfo *linkInfo = [SCLinkInfo new];
                linkInfo.linkURL = result;
                [_messageInfo.links addObject:linkInfo];
                return;
            }
        }
    }
    
    _scanner.scanLocation = savedLocation;      // not a link, rollback scan location
    [_scanner scanString:@"h" intoString:NULL] || [_scanner scanString:@"H" intoString:NULL] ; // and consume first 'h'/'H'
}

- (BOOL)isAfterSeparatorCharacter {
    NSUInteger scanLocation = _scanner.scanLocation;
    if (scanLocation > 0) {
        unichar previous = [_scanner.string characterAtIndex:scanLocation - 1];
        
        NSMutableCharacterSet *separatorCharacterSet = [NSMutableCharacterSet whitespaceAndNewlineCharacterSet];
        [separatorCharacterSet formUnionWithCharacterSet:[NSCharacterSet controlCharacterSet]];
        [separatorCharacterSet formUnionWithCharacterSet:[NSCharacterSet nonBaseCharacterSet]];
        [separatorCharacterSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
        [separatorCharacterSet formUnionWithCharacterSet:[NSCharacterSet symbolCharacterSet]];
        
        if ([separatorCharacterSet characterIsMember:previous]) {
            return YES;
        }
    } else {
        return YES; // no previous character is ok
    }
    return NO;
}

@end

//
//  SCMessageParser.h
//  SimpleChat
//
//  Created by Anton Soloviev on 17/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCMessageInfo.h"


@interface SCMessageParser : NSObject

- (instancetype)initWithMessage:(NSString*) message;

/**
 *  Parse @mentions, (emoticons) and links from message
 *
 *  @return SCMessageInfo with all metadata for message
 */
- (SCMessageInfo*)parse;

@end


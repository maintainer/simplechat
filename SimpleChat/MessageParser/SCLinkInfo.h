//
//  SCLinkInfo.h
//  SimpleChat
//
//  Created by Anton Soloviev on 21/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCLinkInfo : NSObject
@property (nonatomic, copy) NSString *linkURL;
@property (nonatomic, copy) NSString *linkTitle;
@end

//
//  SCMessageInfo
//  SimpleChat
//
//  Created by Anton Soloviev on 17/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import "SCMessageInfo.h"

@implementation SCMessageInfo

- (instancetype)init {
    self = [super init];
    if (self) {
        self.mentions = [NSMutableArray new];
        self.emoticons = [NSMutableArray new];
        self.links = [NSMutableArray new];
    }
    return self;
}

@end

//
//  SCMessageTextView.m
//  SimpleChat
//
//  Created by Anton Soloviev on 17/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import "SCMessageInputPanel.h"


@interface SCMessageInputPanel ()
- (IBAction)tapSenBtn:(id)sender;
@end

@implementation SCMessageInputPanel

- (void)updateHightConstraint {
    CGSize sizeThatFits = [_textView sizeThatFits:_textView.frame.size];
    CGFloat newHeight = sizeThatFits.height;
    newHeight = MIN(newHeight, self.maxHeightConstraint.constant);
    newHeight = MAX(newHeight, self.minHeightConstraint.constant);
    self.heightConstraint.constant = newHeight;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    [self updateHightConstraint];
}

- (void)textViewDidChange:(UITextView *)textView {
    if ([_delegate respondsToSelector:@selector(textDidChange:)]) {
        [_delegate textDidChange:self];
    }
    
    if ([textView.text isEqualToString:@""]) {
        [_sendBtn setEnabled:NO];
    } else {
        [_sendBtn setEnabled:YES];
    }
    
    [self updateHightConstraint];
    [textView layoutIfNeeded];
}

- (IBAction)tapSenBtn:(id)sender {
    [_delegate send:self];
    
    [_textView setText:@""];
    [_sendBtn setEnabled:NO];
    
    [self updateHightConstraint];
    [_textView layoutIfNeeded];
}

@end

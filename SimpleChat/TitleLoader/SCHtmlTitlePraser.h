//
//  SCHtmlTitlePraser.h
//  SimpleChat
//
//  Created by Anton Soloviev on 18/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SCHtmlTitlePraser : NSObject

- (NSString*)parseNextPortion:(NSString*) htmlPortion;

@end

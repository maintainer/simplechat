//
//  SCHtmlTitlePraser.m
//  SimpleChat
//
//  Created by Anton Soloviev on 18/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import "SCHtmlTitlePraser.h"


@implementation SCHtmlTitlePraser {
    NSMutableString *_html;
    NSUInteger _titleBegin;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _html = [NSMutableString string];
        _titleBegin = NSNotFound;
    }
    return self;
}

- (NSString*)parseNextPortion:(NSString*) htmlPortion {
    [_html appendString:htmlPortion];
    _titleBegin = _titleBegin != NSNotFound ? _titleBegin : [self searchTitleBegin];
    if (_titleBegin != NSNotFound) {
        NSUInteger titleLength = [self searchTitleEnd];
        if (titleLength != NSNotFound) {
            return [_html substringWithRange:NSMakeRange(_titleBegin+1, titleLength)];
        }
    }
    if ([_html length] > 1024*1024) { // if more than 1Mb downloaded and there is still no title, then most probably something is wrong
        @throw [NSException exceptionWithName:@"TitlePraserException" reason:@"Title not found" userInfo:nil];
    }
    return nil;
}

// Returns index of symbol before title begin
- (NSUInteger)searchTitleBegin {
    NSRange tagBegin = [_html rangeOfString:@"<title" options:NSCaseInsensitiveSearch];
    if (tagBegin.location != NSNotFound) {
        NSRange tagEndSearch = NSMakeRange(tagBegin.location + tagBegin.length,
                                           _html.length - (tagBegin.location+tagBegin.length));
        NSRange tagEnd = [_html rangeOfString:@">" options:NSLiteralSearch range:tagEndSearch];
        return tagEnd.location;
    }
    return NSNotFound;
}

// Return length of title
- (NSUInteger)searchTitleEnd {
    NSRange tagBegin = [_html rangeOfString:@"</title>" options:NSCaseInsensitiveSearch
                                      range:NSMakeRange(_titleBegin, _html.length - _titleBegin)];
    if (tagBegin.location != NSNotFound) {
        return tagBegin.location - _titleBegin - 1;
    }
    return NSNotFound;
}

@end

//
//  SCTitleLoader.m
//  SimpleChat
//
//  Created by Anton Soloviev on 2016-02-18.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import "SCTitleLoader.h"
#import "SCHtmlTitlePraser.h"


@implementation SCTitleLoader {
    NSURLSession *_session;
    NSMutableDictionary *_handlers;
    NSMutableDictionary *_parsers;
    dispatch_queue_t _handlersAndPrasersQueue;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        _session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
        _handlers = [NSMutableDictionary dictionary];
        _parsers = [NSMutableDictionary dictionary];
        _handlersAndPrasersQueue = dispatch_queue_create("SCTitleLoader.handlersAndPrasersQueue", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

- (void)loadTitleForURL:(NSString*) stringURL completion:(TitleCompletionBlock) completion {
    NSURL *taskURL = [NSURL URLWithString:[stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    __block id handler;
    dispatch_sync(_handlersAndPrasersQueue, ^{
        handler = [_handlers objectForKey:taskURL];
    });
    
    if (!handler) {
        NSURLSessionDataTask *dataTask =  [_session dataTaskWithURL:taskURL];
        
        dispatch_barrier_sync(_handlersAndPrasersQueue, ^{
            [_handlers setObject:completion forKey:taskURL];
            [_parsers setObject:[SCHtmlTitlePraser new] forKey:taskURL];
        });
        
        [dataTask resume];
    } else {
        completion(nil);
    }
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    NSHTTPURLResponse *response = (NSHTTPURLResponse*)dataTask.response;
    
    NSInteger responceSatusCode = [response statusCode];
    if (responceSatusCode / 100 != 2) { // if responce code is not ok or redirect (redirects handled )
        [self completeTask:dataTask title:@""];
        return;
    }
    
    NSRange mimeRange = [response.MIMEType rangeOfString:@"text/html" options:NSCaseInsensitiveSearch];
    if (mimeRange.location == NSNotFound) {
        [self completeTask:dataTask title:@""];
        return;
    }
    
    NSString *encodingName = [dataTask.response textEncodingName];
    encodingName = encodingName != nil ? encodingName : @"UTF-8";
    NSStringEncoding encodingType = CFStringConvertEncodingToNSStringEncoding(CFStringConvertIANACharSetNameToEncoding((CFStringRef)encodingName));
    
    SCHtmlTitlePraser *parser = [_parsers objectForKey:dataTask.originalRequest.URL];
    NSString *nextPortion = [[NSString alloc] initWithData:data encoding:encodingType];
    NSString *title = nil;
    @try {
        title = [parser parseNextPortion:nextPortion];
    }
    @catch (NSException *exception) {
        [self completeTask:dataTask title:@""];
    }
    
    if (title != nil) {
        [self completeTask:dataTask title:title];
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    __block TitleCompletionBlock completion;
    dispatch_sync(_handlersAndPrasersQueue, ^{
        completion = [_handlers objectForKey:task.originalRequest.URL];
    });
    
    if (completion != NULL) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(nil);
        });
    }
    [task cancel];
    dispatch_barrier_async(_handlersAndPrasersQueue, ^{
        [_handlers removeObjectForKey:task.originalRequest.URL];
        [_parsers removeObjectForKey:task.originalRequest.URL];
    });
}

- (void)completeTask:(NSURLSessionDataTask *)dataTask title:(NSString *)title {
    __block TitleCompletionBlock completion;
    dispatch_sync(_handlersAndPrasersQueue, ^{
        completion = [_handlers objectForKey:dataTask.originalRequest.URL];
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        completion(title);
    });
    
    [dataTask cancel];
    
    dispatch_barrier_async(_handlersAndPrasersQueue, ^{
        [_handlers removeObjectForKey:dataTask.originalRequest.URL];
        [_parsers removeObjectForKey:dataTask.originalRequest.URL];
    });
}

- (void)invalidateSession {
    [_session invalidateAndCancel];
}

@end


//
//  SCTitleLoader.h
//  SimpleChat
//
//  Created by Anton Soloviev on 2016-02-18.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^TitleCompletionBlock)(NSString*);


@interface SCTitleLoader : NSObject <NSURLSessionDataDelegate, NSURLSessionTaskDelegate>

/**
 *  Load HTML page and parse a title out of it.
 *  @param stringURL  Page URL
 *  @param completion A block object to be executed when title parsed.
 */
- (void)loadTitleForURL:(NSString*) stringURL completion:(TitleCompletionBlock) completion;

/**
 *  Invalidate the esession object attached to title loader
 *  @discussion
 *  It is very important to invalidate the session attached to a title loader before releasing 
 *  it to avoid reference cycle.
 */
- (void)invalidateSession;

@end

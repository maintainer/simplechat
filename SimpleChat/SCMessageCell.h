//
//  SCMessageCell.h
//  SimpleChat
//
//  Created by Anton Soloviev on 16/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCMessageCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

//
//  ViewController.h
//  SimpleChat
//
//  Created by Anton Soloviev on 16/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCMessageInputPanel.h"
#import "SCChatController.h"


@interface SCChatViewController : UIViewController <SCMessageInputPanelProtocol, SCChatControllerDelegate,
                                                            UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *messagesTable;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *textViewBottomConstraint;

@end


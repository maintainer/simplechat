//
//  SCTitleLoaderTest.swift
//  SimpleChat
//
//  Created by Anton Soloviev on 19/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

import XCTest

let kHTTPRequestsTimeout = 10.0;

class SCTitleLoaderTest: XCTestCase {
    
    var loader: SCTitleLoader!
    
    override func setUp() {
        super.setUp()
        loader = SCTitleLoader()
    }
    
    override func tearDown() {
        loader.invalidateSession()
        super.tearDown()
    }
    
    func testTiteLoadingForRegularHtmlPage() {
        let expectation = expectationWithDescription("testTiteLoadingForRegularHtmlPage")
        loader.loadTitleForURL("https://en.wikipedia.org/wiki/Tone_mapping") { title in
            XCTAssertEqual(title, "Tone mapping - Wikipedia, the free encyclopedia", "Wrong title")
            expectation.fulfill();
        }
        waitForExpectationsWithTimeout(kHTTPRequestsTimeout, handler: nil)
    }
    
    func testTiteLoadingForNonExistingPage() {
        let expectation = expectationWithDescription("testTiteLoadingForNonExistingPage")
        loader.loadTitleForURL("https://en.wikipedia.org/1111") { title in
            XCTAssertEqual(title, "", "Title should be empty if page not available (i.e. 404)")
            expectation.fulfill();
        }
        waitForExpectationsWithTimeout(kHTTPRequestsTimeout, handler: nil)
    }
    
    func testTiteLoadingForRedirectedPage() {
        let expectation = expectationWithDescription("testTiteLoadingForRedirectedPage")
        loader.loadTitleForURL("http://bit.ly/1SiF0jC") { title in
            XCTAssertTrue(title.hasPrefix("Rand Fishkin on Twitter:"), "Wrong title")
            expectation.fulfill();
        }
        waitForExpectationsWithTimeout(kHTTPRequestsTimeout, handler: nil)
    }

    func testTiteLoadingForNonHTMLContent() {
        let expectation = expectationWithDescription("testTiteLoadingForNonHTMLContent")
        loader.loadTitleForURL("https://devimages.apple.com.edgekey.net/assets/elements/icons/tvos-wide/tvos-wide-214x128_2x.png") { title in
            XCTAssertEqual(title, "")
            expectation.fulfill();
        }
        waitForExpectationsWithTimeout(kHTTPRequestsTimeout, handler:  nil)
    }
    
    func testTiteLoadingParallelRequests() {
        let expectation1 = expectationWithDescription("testTiteLoadingParallelRequests1")
        let expectation2 = expectationWithDescription("testTiteLoadingParallelRequests2")
        let expectation3 = expectationWithDescription("testTiteLoadingParallelRequests3")
        
        loader.loadTitleForURL("http://bit.ly/1SiF0jC") { title in
            XCTAssertTrue(title.hasPrefix("Rand Fishkin "), "Wrong title")
            expectation1.fulfill();
        }
        
        loader.loadTitleForURL("https://en.wikipedia.org/wiki/Tone_mapping") { title in
            XCTAssertEqual(title, "Tone mapping - Wikipedia, the free encyclopedia", "Wrong title")
            expectation2.fulfill();
        }
        
        loader.loadTitleForURL("https://en.wikipedia.org/1111") { title in
            XCTAssertEqual(title, "", "Title should be empty if page not available (i.e. 404)")
            expectation3.fulfill();
        }
        
        waitForExpectationsWithTimeout(kHTTPRequestsTimeout, handler:  nil)
    }
}

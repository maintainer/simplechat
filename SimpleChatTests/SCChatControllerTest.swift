//
//  SCChatControllerTest.swift
//  SimpleChat
//
//  Created by Anton Soloviev on 2016-02-22.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

import XCTest

class SCChatControllerTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMessagesSending() {
        let chatController = SCChatController(delegate: nil)
        chatController.sendMessage("Hi @user!")
        chatController.sendMessage("(wave)(sun)")
        XCTAssertEqual(chatController.getMessagesCount(), 2)
        XCTAssert(chatController.getMessageInfo(0).mentions.containsObject("user"))
        XCTAssert(chatController.getMessageInfo(1).emoticons.count == 2)
    }
    
    func testConcurentTasks() {
        let numberOfConcurentRequests = 50;
        
        class ChatControllerDelegate : NSObject, SCChatControllerDelegate {
            var updatesCounter = 0;
            let expectation: XCTestExpectation;

            init(expectation: XCTestExpectation) {
                self.expectation = expectation;
            }
            
            @objc func updateMessage(messageIndex: UInt) {
                updatesCounter++;
                if updatesCounter >= 50 {
                    expectation.fulfill();
                }
            }
        }
        
        let expectAllMessagesUpdated = expectationWithDescription("expectAllMessagesUpdated")
        let controllerDelegate = ChatControllerDelegate(expectation: expectAllMessagesUpdated)
        let chatController = SCChatController(delegate: controllerDelegate)
        
        for i in 1...numberOfConcurentRequests {
            chatController.sendMessage("http://somebrokenlink.nodomain/\(i)");
        }
        
        waitForExpectationsWithTimeout(120, handler: nil)
        XCTAssertTrue(controllerDelegate.updatesCounter == numberOfConcurentRequests)
    }

}

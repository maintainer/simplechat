//
//  SCMessageParserTest.swift
//  SimpleChat
//
//  Created by Anton Soloviev on 17/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

import XCTest

class SCMessageParserTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testValidMentions() {
        let validMentions = [("Hi @user!", ["user"]),
                            ("Привет @пользователь!", ["пользователь"]),
                            ("hey,@superuser", ["superuser"]),
                            ("(@user1 and @user2)", ["user1", "user2"]),
                            ("#%@user3 *&@user4", ["user3", "user4"]),
                            (".$@user5 @user6 ,@user7", ["user5", "user6", "user7"]),
        ]
        
        for (msg, expectedResult) in validMentions {
            let parser = SCMessageParser(message: msg)
            let metadata = parser.parse()
            XCTAssertEqual(metadata.mentions, expectedResult)
            XCTAssert(metadata.emoticons.count == 0, "Unexpected emoticons \(metadata.emoticons) found in message '\(msg)'.")
            XCTAssert(metadata.links.count == 0, "Unexpected links \(metadata.links) found in message '\(msg)'.")
        }
    }
    
    func testInvalidMentions() {
        let invalidMentions = ["aa@22", "@@@@@", "email: user@server.com", "user@", "@"]
        
        for msg in invalidMentions {
            let parser = SCMessageParser(message: msg)
            let metadata = parser.parse()
            XCTAssert(metadata.mentions.count == 0, "Test message with invalid mention '\(msg)' failed.")
            XCTAssert(metadata.emoticons.count == 0, "Unexpected emoticons \(metadata.emoticons) found in message '\(msg)'.")
            XCTAssert(metadata.links.count == 0, "Unexpected links \(metadata.links) found in message '\(msg)'.")
        }
    }

    func testValidEmoticons() {
        let validEmoticons  = [("Wow!!!(wow)!!!", ["wow"]),
                              ("(солнце)", ["солнце"]),
                              ("(sunny)(day)", ["sunny", "day"]),
                              ("(good) (night)", ["good", "night"]),
        ]

        for (msg, expectedResult) in validEmoticons {
            let parser = SCMessageParser(message: msg)
            let metadata = parser.parse()
            XCTAssertEqual(metadata.emoticons, expectedResult)
            XCTAssert(metadata.mentions.count == 0, "Unexpected mentions \(metadata.mentions) found in message '\(msg)'.")
            XCTAssert(metadata.links.count == 0, "Unexpected links \(metadata.links) found in message '\(msg)'.")
        }
    }
    
    func testInvalidEmoticons() {
        let invalidEmoticons = ["(", "()", "(((()", "(very(weird)emoticon)", "(hi there!)", "(bye bye)",
            "(emoticonwithverylongname)", "(emotic@n)"]
        
        for msg in invalidEmoticons {
            let parser = SCMessageParser(message: msg)
            let metadata = parser.parse()
            XCTAssert(metadata.emoticons.count == 0, "Test message with invalid emoticon '\(msg)' failed.")
            XCTAssert(metadata.mentions.count == 0, "Unexpected mensions \(metadata.mentions) found in message '\(msg)'.")
            XCTAssert(metadata.links.count == 0, "Unexpected links \(metadata.links) found in message '\(msg)'.")
        }
    }

    func testValidLinks() {
        let validLinks = [("http://some.link - check this out!", ["http://some.link"]),
                        ("more information on the site:https://a.good.one", ["https://a.good.one"]),
                        ("(http://google.com", ["http://google.com"]),
                        ("https://user:password@host.com/valid/link",["https://user:password@host.com/valid/link"]),
                        ("https://www.google.com/?q=最好的2015年的電影",["https://www.google.com/?q=最好的2015年的電影"]),
                        ("https://www.google.com/?q=بہترین+فلموں+2015", ["https://www.google.com/?q=بہترین+فلموں+2015"]),
                        ("HTTPS://apple.com", ["https://apple.com"]),
                        ("Search engines: https://google.com https://bing.com", ["https://google.com","https://bing.com"])
        ]
        
        for (message, expectedResult) in validLinks {
            let parser = SCMessageParser(message: message)
            let metadata = parser.parse()
            
            for linkInfo in metadata.links {
                XCTAssert(expectedResult.contains(linkInfo.linkURL))
            }
            XCTAssert(metadata.emoticons.count == 0, "Unexpected emoticons '\(metadata.emoticons)' found in message '\(message)'.")
            XCTAssert(metadata.mentions.count == 0, "Unexpected mensions \(metadata.mentions) found in message '\(message)'.")
        }
    }

    func testInvalidLinks() {
        let invalidLinks = ["httpp://some.site", "some texthttp://some.site", "http://?that's#not*a$link@^."]
        
        for message in invalidLinks {
            let parser = SCMessageParser(message: message)
            let metadata = parser.parse()
            XCTAssert(metadata.links.count == 0, "Test message with invalid link '\(message)' failed.")
        }
    }

    func testMixedFeatures() {
        let parser = SCMessageParser(message: "@user!(happy)https://bing.com")
        let metadata = parser.parse()
        XCTAssert(metadata.links.count == 1)
        XCTAssert(metadata.emoticons.count == 1)
        XCTAssert(metadata.mentions.count == 1)
    }
}

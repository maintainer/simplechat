//
//  SCTitleParserTest.swift
//  SimpleChat
//
//  Created by Anton Soloviev on 18/02/2016.
//  Copyright © 2016 Anton Soloviev. All rights reserved.
//

import XCTest

class SCTitleParserTest: XCTestCase {
    
    func testWholeTitleInOnePortion() {
        let parser = SCHtmlTitlePraser()
        let title = parser.parseNextPortion("<html><title attribure='something'>The Linux Kernel Archive</title>")
        XCTAssertEqual(title, "The Linux Kernel Archive", "Title parsed incorrectly")
    }
    
    func testTitlePotions() {
        let parser = SCHtmlTitlePraser()
        var title = parser.parseNextPortion("<html><t")
        XCTAssertNil(title, "Parser returned result before title parsing finished")
        title = parser.parseNextPortion("itle>The Linux Kerne")
        XCTAssertNil(title, "Parser returned result before title parsing finished")
        title = parser.parseNextPortion("l Archive</titl")
        XCTAssertNil(title, "Parser returned result before title parsing finished")
        title = parser.parseNextPortion("e>")
        XCTAssertEqual(title, "The Linux Kernel Archive", "Title parsed incorrectly")
    }
}
